class User < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :email, :username, :address, :age, :birthday, 
    :country_id, :password, :password_confirmation
  attr_accessor :password

  has_many :articles, :dependent => :destroy
  has_many :title_articles,
    :class_name => "Article" ,
    :foreign_key => "user_id" ,
    :conditions => "title = 'test title'"

  has_many :products, :dependent => :destroy
  belongs_to :country

  before_save :encrypt_password

  validates :username,  :presence => true,
    :length => {:minimum => 1, :maximum => 20},
    :format => { :with => /\A[a-zA-Z]+\z/},
    :allow_nil => false,
    :uniqueness => true
  validates :email, :presence => true,
    :uniqueness => true,
    :allow_nil => false,
    :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
  validates :password, :presence => {:on => :create},
    :confirmation => true

  def get_full_address
    "#{self.address} #{self.country.name}"
  end

  def is_admin
    return self.email == "hendrik.saragih@kiranatama.com"
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
end