class Comment < ActiveRecord::Base
  attr_accessible :content, :article_id
  validates :content,  :presence => true
  belongs_to :article
end
