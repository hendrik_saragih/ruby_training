class UsersController < ApplicationController
  before_filter :require_login, :only => [:destroy, :edit, :update, :show]
  
  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    if verify_recaptcha
      if @user.save
        UserMailer.registration_confirmation(@user).deliver
        redirect_to root_path, :notice => "Signed up!"
      else
        render "new"
      end
    else
      flash[:error] = "There was an error with the recaptcha code below.
                     Please re-enter the code."
      render "new"
    end
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      flash[:notice] = "Data was successfully saved"
      redirect_to user_path(params[:id])
    else
      render "edit"
    end
  end

  def edit
    @user = User.find(params[:id])
    @user.birthday = @user.birthday.strftime("%d-%m-%Y")
  end
end
