class Product < ActiveRecord::Base
  attr_accessible :name, :price, :description, :user_id, :weight, :category_id
  
  belongs_to :user
  belongs_to :category

  validates :name,  :presence => true,
    :uniqueness => true

  validates :price,  :numericality => true

  validates :weight,  :numericality => true

  validates :description,  :presence => true
  
  validates :category_id,  :presence => true

  validates :user_id,  :presence => true
  
  def get_category
    "#{self.category.name}"
  end
end
