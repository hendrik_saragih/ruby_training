class Article < ActiveRecord::Base
  attr_accessible :title, :body, :rating, :user_id
  belongs_to :user
  has_many :comments, :dependent => :destroy

  scope :certain_rating, lambda {|rate| where("rating = ?", rate) }

  validates :title,  :presence => true,
    :uniqueness => true,
    :allow_blank => false,
    :exclusion => { :in => %w(nil empty blank)}

  validates :rating,  :numericality => true

  validates :body,  :presence => true

  def self.more_than_ten_char
    where("length(body)>10")
  end
end
