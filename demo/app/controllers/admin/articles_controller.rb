class Admin::ArticlesController < Admin::ApplicationController
  before_filter :require_admin_login
  layout 'admin'
  before_filter :check_user, :only => [:destroy]

  def check_user
    @article = Article.find(params[:id])
    if @article.user_id != session[:user_id]
      flash[:notice] = "You are not permitted delete that article "
      @articles = Article.all
      render "index"
    end
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(params[:article])
    if @article.save
      flash[:notice] = "Data was successfully saved"
      redirect_to admin_articles_path
    else
      render new_admin_article_path
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])
    if @article.update_attributes(params[:article])
      flash[:notice] = "Data was successfully saved"
      redirect_to admin_articles_path
    else
      render new_admin_article_path
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    flash[:notice] = "Data was successfully deleted"
    redirect_to admin_articles_path
  end

  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
    @comments = @article.comments
  end
end
