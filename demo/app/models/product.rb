class Product < ActiveRecord::Base
  attr_accessible :name, :price, :description, :user_id
  belongs_to :user
  has_many :categories, :through => :categories_products
  has_many :categories_products

  scope :price_greater_than_thousand, where("price>1000")
  scope :red, where("name like '%red%'")

  validates :name,  :presence => true,
    :uniqueness => true
  
  validates :price,  :numericality => true

  validates :description,  :presence => true
end
