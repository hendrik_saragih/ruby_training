class ArticlesController < ApplicationController
  before_filter :require_login, :only => [:destroy, :edit, :new, :update, :create]
  before_filter :check_user, :only => [:destroy, :edit, :update]
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  def check_user
    @article = Article.find(params[:id])
    if !is_owner(@article.user_id)
      flash[:notice] = "You are not permitted"
      @articles = Article.all
      render "index"
    end
  end

  def new
    @article = Article.new    
  end

  def create
    @article = Article.new(params[:article])
    if @article.save
      flash[:notice] = "Data was successfully saved"
      redirect_to articles_path
    else
      flash[:error] = "Article was failed to create."
      render new_article_path
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])
    if @article.update_attributes(params[:article])
      flash[:notice] = "Article was successfully updated."
      redirect_to articles_path
    else
      flash[:error] = "Article was failed to update."
      render "edit"
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    flash[:notice] = "Data was successfully deleted"
    redirect_to articles_path
  end

  def index
    @articles = Article.order('id DESC').all
  end

  def show
    @article = Article.find(params[:id])
    @comments = @article.comments.order('id ASC')
  end

  private
  def record_not_found
    flash[:notice] = 'Cannot find the article'
    redirect_to articles_path
  end
end
