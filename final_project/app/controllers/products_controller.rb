class ProductsController < ApplicationController
  before_filter :require_login, :only => [:destroy, :edit, :new, :update, :create]
  before_filter :check_user, :only => [:destroy, :edit, :update]
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  
  def check_user
    @product = Product.find(params[:id])
    if !is_owner(@product.user_id)
      flash[:notice] = "You are not permitted"
      @products = Product.all
      render "index"
    end
  end

  def index
    @products = Product.order('id DESC').all
  end

  def new
    @product = Product.new
    @category = Category.all
  end

  def create
    @product = Product.new(params[:product])
    if @product.save
      flash[:notice] = "Data was successfully saved"
      redirect_to products_path
    else
      flash[:error] = 'Product was failed to create.'
      @category = Category.all
      render new_product_path
    end
  end

  def edit
    @product = Product.find(params[:id])
    @category = Category.all
  end

  def update
    @product = Product.find(params[:id])
    if @product.update_attributes(params[:product])
      flash[:notice] = "Product was successfully updated."
      redirect_to products_path
    else
      flash[:error] = 'Product was failed to update.'
      @category = Category.all
      render "edit"
    end
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    flash[:notice] = "Data was successfully deleted"
    redirect_to products_path
  end
  
  def show
    @product = Product.find(params[:id])
  end

  private
  def record_not_found
    flash[:notice] = 'Cannot find the product'
    redirect_to products_path
  end
end
