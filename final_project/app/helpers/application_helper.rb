module ApplicationHelper
  def menu_sign_up
    str = "<li>"
    if current_user
      str += '<a href="/">Home</a>'
    else
      str += '<a href="/sign_up">Sign Up</a>'
    end
    str += "</li>"
  end
end
