class ProductsController < ApplicationController
  def new
    @product = Product.new
    @users = User.all
  end

  def create
    @product = Product.new(params[:product])
    if @product.save
      flash[:notice] = "Data was successfully saved"
      redirect_to products_path
    else
      render new_product_path
    end
  end

  def edit
    @product = Product.find(params[:id])
    @users = User.all
  end

  def update
    @product = Product.find(params[:id])
    if @product.update_attributes(params[:product])
      flash[:notice] = "Data was successfully saved"
      redirect_to products_path
    else
      render new_product_path
    end
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    flash[:notice] = "Data was successfully deleted"
    redirect_to products_path
  end

  def index
    @products = Product.all
  end
end
