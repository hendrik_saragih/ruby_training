class UserMailer < ActionMailer::Base
  default from: "hendrik.saragih@kiranatama.com"
  #  :default_from => "eifion@asciicasts.com"
  def registration_confirmation(user)
    @user = user
    mail(:to => user.email, :subject => "Registered")
  end
end
