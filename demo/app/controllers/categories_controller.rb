class CategoriesController < ApplicationController
  def new
    @category = Category.new
  end

  def create
    @category = Category.new(params[:category])
    if @category.save
      flash[:notice] = "Data was successfully saved"
      redirect_to categories_path
    else
      render new_category_path
    end
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])
    if @category.update_attributes(params[:category])
      flash[:notice] = "Data was successfully saved"
      redirect_to categories_path
    else
      render new_category_path
    end
  end

  def destroy
    @category = Category.find(params[:id])
    @category.destroy
    flash[:notice] = "Data was successfully deleted"
    redirect_to categories_path
  end

  def index
    @categories = Category.all
  end
end
