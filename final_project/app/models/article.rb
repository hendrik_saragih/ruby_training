class Article < ActiveRecord::Base
  attr_accessible :title, :body, :user_id
  belongs_to :user
  has_many :comments, :dependent => :destroy

  validates :title,  :presence => true,
    :uniqueness => true

  validates :body,  :presence => true

  def get_user
    "#{self.user.name}"
  end

end
