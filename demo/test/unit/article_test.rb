require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  
  def test_save_without_title
    article = Article.new(:body => 'new_body')
    assert_equal article.valid?, false
    assert_equal article.save, false
  end

  def test_save_without_body
    article = Article.new(:title => 'new title')
    assert_equal article.valid?, false
    assert_equal article.save, false
  end


  def test_save_with_title_and_body
    article = Article.new(:title => "Testing", :body => "This is body", :rating=>1)
    assert_equal article.valid?, true
    assert_equal article.save, true
  end

  def test_more_than_ten_char
    Article.create([
        {:title => "share 1", :body => "test posting", :rating => 3}, {:title => "test2", :body => "test
      posting2", :rating => 5},
      ])
    assert_not_nil Article.more_than_ten_char
    assert_equal Article.more_than_ten_char[0].title, "share 1"
  end

  def test_relation_between_article_and_comment
    article = Article.create(:title => "new_title", :body => "new content", :rating=>1)
    assert_not_nil article
    comment = Comment.create(:article_id => article.id, :content => "my comment")
    assert_not_nil article.comments
    assert_equal article.comments.empty?, false
    assert_equal article.comments[0].class, Comment
  end
end
