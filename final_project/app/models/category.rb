class Category < ActiveRecord::Base
  attr_accessible :name, :parent_id

  has_many :products, :dependent => :destroy

  validates :name,  :presence => true,
    :uniqueness => true,
    :exclusion => { :in => %w(nil empty blank)}

  def get_parent_category
    if self.parent_id
      "#{Category.find(self.parent_id).name}"
    else
      ""
    end
    
  end
end
