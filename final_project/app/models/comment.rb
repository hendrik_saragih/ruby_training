class Comment < ActiveRecord::Base
  attr_accessible :comment, :article_id, :user_id
  validates :comment,  :presence => true
  belongs_to :article
end