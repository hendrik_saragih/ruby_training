class Category < ActiveRecord::Base
  attr_accessible :name
  has_many :products, :through => :categories_products
  has_many :categories_products

  validates :name,  :presence => true,
    :uniqueness => true,
    :exclusion => { :in => %w(nil empty blank)}
end
