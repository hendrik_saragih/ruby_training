class CreateCountries < ActiveRecord::Migration
  def up
    create_table :countries do |t|
      t.integer  :code
      t.string  :name
    end
  end

  def down
    drop_table :countries
  end
end
