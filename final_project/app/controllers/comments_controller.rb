class CommentsController < ApplicationController
  before_filter :require_login, :only => [:create]
  
  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(params[:comment])

    respond_to do |format|
      if @comment.valid?
        flash[:notice] = "Comment succesfully added"
        if !current_user.is_admin
          format.html { redirect_to(article_path(@article)) }
        else
          format.html { redirect_to(article_path(@article)) }
        end

        format.js { @comments = Article.find(params[:comment][:article_id].to_i).comments }
      end
    end

  end
end
