class Admin::ApplicationController < ApplicationController
  protect_from_forgery
  helper_method :is_administrator

  def require_admin_login
    if !is_administrator
      flash[:error] = "Only admins are permitted"
      redirect_to root_path
    else
      return current_user
    end
  end

  private
  def is_administrator
    return !current_user.nil? && current_user.is_admin
  end
end