require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  def test_save_without_name
    product = Product.new(:price => 20)
    assert_equal product.valid?, false
    assert_equal product.save, false
  end

  def test_save_without_description
    product = Product.new(:price => 20)
    assert_equal product.valid?, false
    assert_equal product.save, false
  end

  def test_save_without_category_id
    product = Product.new(:price => 20)
    assert_equal product.valid?, false
    assert_equal product.save, false
  end

  def test_save_without_user_id
    product = Product.new(:price => 20)
    assert_equal product.valid?, false
    assert_equal product.save, false
  end

  def test_save_without_numeric_price
    product = Product.new(:price => 'asdasd')
    assert_equal product.valid?, false
    assert_equal product.save, false
  end

  def test_save_without_numeric_weight
    product = Product.new(:weight => 'asdasd')
    assert_equal product.valid?, false
    assert_equal product.save, false
  end

  def test_save
    product = Product.new(:name => "Testing", :description => "desc", :price=>10, :weight=>30,
      :category_id=>1, :user_id=>1)
    assert_equal product.valid?, true
    assert_equal product.save, true
  end

end
