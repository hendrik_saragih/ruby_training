class User < ActiveRecord::Base
  attr_accessible :password, :password_confirmation, :email, :gender,
    :name, :address, :phone_number, :birthday, :facebook, :blog
  attr_accessor :password

  before_save :encrypt_password

  has_many :products, :dependent => :destroy
  
  has_many :articles, :dependent => :destroy

  validates :name,  :presence => true,
    :length => {:minimum => 1, :maximum => 20},
    :format => { :with => /\A[a-zA-Z]+\z/},
    :allow_nil => false,
    :uniqueness => true
  validates :email, :presence => true,
    :uniqueness => true,
    :allow_nil => false,
    :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
  validates :password, :presence => {:on => :create},
    :confirmation => true
  
  def is_admin
    return self.email == "admin@admin.com"
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
end
