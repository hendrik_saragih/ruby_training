# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

articles = Article.create([
    {:title=>"title 1", :body=>"asdasd asd ad asd as das da sd"},
    {:title=>"title 2", :body=>"asdasd asd ad asd as das da sd"},
    {:title=>"title 3", :body=>"asdasd asd ad asd as das da sd"},
    {:title=>"title 4", :body=>"asdasd asd ad asd as das da sd"},
    {:title=>"title 5", :body=>"asdasd asd ad asd as das da sd"}
  ])

users = User.create([
    {:first_name => "Adalbert", :last_name => "George", :email=>"adalber@email.com", :username=>"adalbert", :address=>"adress adalbert", :age=>30, :birthday=>"1982-03-12"},
    {:first_name => "Bogimilus", :last_name => "Theophilus", :email=>"bogimilus@email.com", :username=>"bogimilus", :address=>"adress bogimilus", :age=>60, :birthday=>"1952-02-01"},
    {:first_name => "Boleslaus", :last_name => "William", :email=>"boleslaus@email.com", :username=>"boleslaus", :address=>"adress boleslaus", :age=>20, :birthday=>"1992-05-02"},
    {:first_name => "Emiliana", :last_name => "Emily", :email=>"emily@email.com", :username=>"emily", :address=>"adress emily", :age=>35, :birthday=>"1977-06-02"},
    {:first_name => "Felicity", :last_name => "Frederick", :email=>"felicity@email.com", :username=>"felicity", :address=>"adress felicity", :age=>24, :birthday=>"1988-04-04"}
  ])


