class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user, :is_administrator, :is_owner

  def require_login
    if current_user.nil?
      flash[:error] = "You are not permitted, please login first"
      redirect_to root_path
    else
      return current_user
    end
  end

  private
  def is_owner(user)
    return !current_user.nil? && current_user.id == user
  end
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  
  
end
