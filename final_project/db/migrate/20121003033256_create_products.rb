class CreateProducts < ActiveRecord::Migration
  def up
    create_table :products do |t|
      t.string  :name
      t.text  :description
      t.decimal  :price
      t.decimal  :weight
      t.integer  :user_id
      t.integer  :category_id
    end
  end

  def down
    drop_table :products
  end
end
