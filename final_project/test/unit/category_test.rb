require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  def test_save_without_name
    category = Category.new(:parent_id => nil)
    assert_equal category.valid?, false
    assert_equal category.save, false
  end

  def test_save
    category = Category.new(:parent_id => 1, :name => "new category")
    assert_equal category.valid?, true
    assert_equal category.save, true
  end
end
