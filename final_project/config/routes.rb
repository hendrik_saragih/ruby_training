FinalProject::Application.routes.draw do
  
  get "log_in" => "sessions#new", :as => "log_in"

  get "log_out" => "sessions#destroy", :as => "log_out"

  get "sign_up" => "users#new", :as => "sign_up"
  
  root :to => "products#dashboard"

  resources :products, :users, :sessions
  
  resources :articles do
    resources :comments
  end

  namespace :admin do
    resources :categories
  end
end
