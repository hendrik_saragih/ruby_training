class Admin::CategoriesController < Admin::ApplicationController
  before_filter :require_admin_login
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  
  def new
    @category = Category.new
    @parent_category = Category.where(["parent_id IS NULL"])
  end

  def create
    @category = Category.new(params[:category])
    if @category.save
      flash[:notice] = "Data was successfully saved"
      redirect_to admin_categories_path
    else
      flash[:error] = 'Category was failed to create.'
      @parent_category = Category.where(["parent_id IS NULL"])
      render new_admin_category_path
    end
  end

  def edit
    @category = Category.find(params[:id])
    @parent_category = Category.where(["parent_id IS NULL"])
  end

  def update
    @category = Category.find(params[:id])
    if @category.update_attributes(params[:category])
      flash[:notice] = "Data was successfully saved"
      redirect_to admin_categories_path
    else
      flash[:error] = "Data was failed to update."
      @parent_category = Category.where(["parent_id IS NULL"])
      render "edit"
    end
  end

  def destroy
    @category = Category.find(params[:id])
    @category.destroy
    flash[:notice] = "Data was successfully deleted"
    redirect_to admin_categories_path
  end

  def index
    @categories = Category.order('id DESC').all
  end

  def show
    @category = Category.find(params[:id])
  end

  private
  def record_not_found
    flash[:notice] = 'Cannot find the category'
    redirect_to admin_categories_path
  end
end
