class Country < ActiveRecord::Base
  attr_accessible :code, :name
  has_many :users
  
  validates :code,  :presence => true,
    :inclusion => { :in => %w(id usa frc)}
end
